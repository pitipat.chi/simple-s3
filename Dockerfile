FROM maven:3-jdk-8-slim as builder
ENV MAIN_CLASS=io/muic/s3/S3Application.kt
ARG BASE_PACKAGE=src/main/kotlin

WORKDIR /app
ADD pom.xml .
COPY $BASE_PACKAGE/$MAIN_CLASS $BASE_PACKAGE/$MAIN_CLASS
RUN mvn -U clean package -Dmaven.test.skip=true
ADD src src
RUN mvn -U package

FROM openjdk:8-alpine
ENV JAR_FILE=s3-0.0.1-SNAPSHOT.jar
WORKDIR /app
COPY --from=builder /app/target/$JAR_FILE $JAR_FILE
ENV ENV_JAR_FILE ${JAR_FILE}
EXPOSE 8080
CMD java -jar $ENV_JAR_FILE
