package io.muic.s3.controllers

import org.junit.jupiter.api.*

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("Test S3 Object Meta-data Controller Layer")
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class S3MetaDataControllerTest {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private lateinit var mockMvc: MockMvc

    private val bucketName: String = "test-bucket-1"
    private val notExistsBucketName: String = "not-exists-bucket"

    private val objName: String = "test-obj-1"
    private val notExistsObjName: String = "not-exists-obj"

    @BeforeAll
    fun init() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$bucketName?create")
        ).andDo {
            mockMvc.perform(MockMvcRequestBuilders
                    .post("http://localhost:$port/$bucketName/$objName?create")
            )
        }
    }

    @Test
    @Order(1)
    fun updateKey() {
        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?metadata&key=key")
                .content("key")
        )
                .andExpect(status().isOk)


        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$notExistsBucketName/$objName?metadata&key=key")
                .content("key")
        )
                .andExpect(status().isNotFound)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$notExistsObjName?metadata&key=key")
                .content("key")
        )
                .andExpect(status().isNotFound)
    }

    @Test
    @Order(2)
    fun getKey() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName?metadata&key=key")
        )
                .andExpect(status().isOk)


        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$notExistsBucketName/$objName?metadata&key=key")
        )
                .andExpect(status().isNotFound)

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$notExistsObjName?metadata&key=key")
        )
                .andExpect(status().isNotFound)
    }

    @Test
    @Order(4)
    fun deleteKey() {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$bucketName/$objName?metadata&key=key")
        )
                .andExpect(status().isOk)


        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$notExistsBucketName/$objName?metadata&key=key")
        )
                .andExpect(status().isNotFound)

        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$bucketName/$notExistsObjName?metadata&key=key")
        )
                .andExpect(status().isNotFound)
    }

    @Test
    @Order(3)
    fun get() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName?metadata")
        )
                .andExpect(status().isOk)


        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$notExistsBucketName/$objName?metadata")
        )
                .andExpect(status().isNotFound)

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$notExistsObjName?metadata")
        )
                .andExpect(status().isNotFound)
    }
}