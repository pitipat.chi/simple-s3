package io.muic.s3.controllers

import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*


@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("Test S3 Bucket Controller Layer")
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
internal class S3BucketControllerTest {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private lateinit var mockMvc: MockMvc

    private val name: String = "test-bucket"
    private val notExistsName: String = "not-exists-bucket"

    @Test
    @Order(value = 1)
    fun create() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$name?create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.created").exists())
                .andExpect(jsonPath("$.modified").exists())
                .andDo {
                    mockMvc.perform(MockMvcRequestBuilders
                            .post("http://localhost:$port/$name?create")
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
                    )
                            .andExpect(status().isBadRequest)
                }
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$name\$?create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(value = 2)
    fun list() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$name?list"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.created").exists())
                .andExpect(jsonPath("$.modified").exists())
                .andExpect(jsonPath("$.objects").exists())

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$notExistsName?list"))
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(value = 3)
    fun delete() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$name?list"))
                .andExpect(status().isOk)

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$notExistsName?list"))
                .andExpect(status().isBadRequest)
    }

}
