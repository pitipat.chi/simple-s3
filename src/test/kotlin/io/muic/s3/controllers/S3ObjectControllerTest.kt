package io.muic.s3.controllers

import com.google.gson.Gson
import org.apache.commons.codec.digest.DigestUtils
import org.junit.jupiter.api.*

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("Test S3 Object Controller Layer")
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class S3ObjectControllerTest {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private lateinit var mockMvc: MockMvc

    private val bucketName: String = "test-bucket-2"
    private val notExistsBucketName: String = "not-exists-bucket"

    private val objName: String = "test-obj-2"
    private val notExistsObjName: String = "not-exists-obj"

    private val content1: String = "aaaaa"
    private val content2: String = "bbbbb"
    private val content3: String = "ccccc"

    private var etag: String = ""

    @BeforeAll
    fun init() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$bucketName?create")
        )
    }

    @Test
    @Order(1)
    fun create() {
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$bucketName/$objName?create")
        )
                .andExpect(status().isOk)

        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$bucketName/$objName?create")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$notExistsBucketName/$objName?create")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$notExistsBucketName/$objName\$.?create")
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(7)
    fun delete() {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$notExistsBucketName/$objName?delete")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$bucketName/$notExistsObjName?delete")
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(2)
    fun upload() {
        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=1")
                .header("Content-Length", content1.length)
                .header("Content-MD5", DigestUtils.md5Hex(content1).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content1)
        )
                .andExpect(status().isOk)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=2")
                .header("Content-Length", content2.length)
                .header("Content-MD5", DigestUtils.md5Hex(content2).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content2)
        )
                .andExpect(status().isOk)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=0")
                .header("Content-Length", content3.length)
                .header("Content-MD5", DigestUtils.md5Hex(content3).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content3)
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=4")
                .header("Content-Length", content3.length)
                .header("Content-MD5", DigestUtils.md5Hex(content2).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content3)
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=4")
                .header("Content-Length", content3.length+1)
                .header("Content-MD5", DigestUtils.md5Hex(content3).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content3)
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$notExistsBucketName/$objName?partNumber=4")
                .header("Content-Length", content3.length)
                .header("Content-MD5", DigestUtils.md5Hex(content3).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content3)
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$notExistsObjName?partNumber=4")
                .header("Content-Length", content3.length)
                .header("Content-MD5", DigestUtils.md5Hex(content3).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content3)
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=3")
                .header("Content-Length", content3.length)
                .header("Content-MD5", DigestUtils.md5Hex(content3).toLowerCase())
                .contentType(MediaType.TEXT_PLAIN)
                .content(content3)
        )
                .andExpect(status().isOk)
    }

    @Test
    @Order(3)
    fun deletePart() {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$bucketName/$objName?partNumber=4")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$notExistsBucketName/$objName?partNumber=3")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$bucketName/$notExistsObjName?partNumber=3")
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(4)
    fun complete() {
        val result = mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$bucketName/$objName?complete")
        )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.etag").value(
                        DigestUtils.md5Hex(DigestUtils.md5Hex(content1) + DigestUtils.md5Hex(content2) + DigestUtils.md5Hex(content3))+"-3")
                )
                .andExpect(jsonPath("$.length").value(
                        content1.length.toLong() + content2.length.toLong() + content3.length.toLong()
                ))
                .andReturn()
        val json: Map<*, *> = Gson().fromJson(result.response.contentAsString, Map::class.java)
        etag = "\"${json["etag"] as String}\""
        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$notExistsBucketName/$objName?complete")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .post("http://localhost:$port/$bucketName/$notExistsObjName?complete")
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(5)
    fun afterComplete() {
        mockMvc.perform(MockMvcRequestBuilders
                .delete("http://localhost:$port/$bucketName/$objName?partNumber=1")
        )
                .andExpect(status().isBadRequest)

        mockMvc.perform(MockMvcRequestBuilders
                .put("http://localhost:$port/$bucketName/$objName?partNumber=3")
                .header("Content-Length", content3.length)
                .header("Content-MD5", DigestUtils.md5Hex(content3).toLowerCase())
                .content(content3)
        )
                .andExpect(status().isBadRequest)
    }

    @Test
    @Order(6)
    fun download() {
        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
        )
                .andExpect(status().isOk)
                .andExpect(header().exists("ETag"))
                .andExpect(content().bytes("aaaaabbbbbccccc".toByteArray()))

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
                .header("If-None-Match", etag)
        )
                .andExpect(status().isNotModified)

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
                .header("Range", "bytes=0-5")
        )
                .andExpect(status().isOk)
                .andExpect(content().bytes("aaaaab".toByteArray()))

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
                .header("Range", "bytes=6")
        )
                .andExpect(status().isOk)
                .andExpect(content().bytes("aaaaab".toByteArray()))

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
                .header("Range", "bytes=-6")
        )
                .andExpect(status().isOk)
                .andExpect(content().bytes("bccccc".toByteArray()))

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
                .header("Range", "bytes=1-")
        )
                .andExpect(status().isOk)
                .andExpect(content().bytes("aaaabbbbbccccc".toByteArray()))

        mockMvc.perform(MockMvcRequestBuilders
                .get("http://localhost:$port/$bucketName/$objName")
                .header("Range", "bytes=1-, 3, -3")
        )
                .andExpect(status().isOk)
                .andExpect(content().bytes("aaaabbbbbcccccaaaccc".toByteArray()))
    }
}