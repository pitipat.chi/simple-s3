package io.muic.s3.exceptions.part

class S3PartInvalidObjectNameException(
        bucketName: String,
        objName: String,
        partNumber: Int = 0,
        partError: PartErrorDTO? = null,
        objectError: ObjectErrorDTO? = null
)
    : S3PartException(
        objError = objectError,
        partError = partError,
        partNumber = partNumber,
        msg = "${bucketName}/${objName}?partNumber=${partNumber}: Invalid Object Name '${objName}'."
){
}