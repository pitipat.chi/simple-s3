package io.muic.s3.exceptions.part

data class PartErrorDTO (
        val md5: String,
        val length: Long
)
