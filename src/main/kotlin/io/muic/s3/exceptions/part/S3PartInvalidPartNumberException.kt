package io.muic.s3.exceptions.part

class S3PartInvalidPartNumberException(
        bucketName: String,
        objName: String,
        partNumber: Int = 0,
        partError: PartErrorDTO? = null
)
    : S3PartException(
        partNumber = partNumber,
        partError = partError,
        msg = "${bucketName}/${objName}?partNumber=${partNumber}: Invalid Part Number '${partNumber}'. Must be in between 1 and 10000 inclusive."
){
}