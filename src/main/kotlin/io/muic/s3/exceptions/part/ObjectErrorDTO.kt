package io.muic.s3.exceptions.part

data class ObjectErrorDTO (
        val bucket: String,
        val name: String
)
