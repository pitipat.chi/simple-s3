package io.muic.s3.exceptions.part

import java.lang.RuntimeException

abstract class S3PartException(
        val partError: PartErrorDTO? = null,
        val objError: ObjectErrorDTO? = null,
        val partNumber: Int = 0,
        msg: String
): RuntimeException(msg)