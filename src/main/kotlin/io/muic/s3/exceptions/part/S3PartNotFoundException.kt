package io.muic.s3.exceptions.part

class S3PartNotFoundException(
        bucketName: String,
        objName: String,
        partNumber: Int = 0,
        partError: PartErrorDTO? = null
)
    : S3PartException(
        partNumber = partNumber,
        partError = partError,
        msg = "${bucketName}/${objName}?partNumber=${partNumber}: Not found."
){
}