package io.muic.s3.exceptions.bucket

import java.lang.RuntimeException

class S3BucketInvalidBucketNameException(name: String) : RuntimeException("Name '${name}' is invalid.")
