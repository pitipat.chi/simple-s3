package io.muic.s3.exceptions.bucket

import java.lang.RuntimeException

class S3BucketNotFoundException(name: String) : RuntimeException("Bucket '${name}' not found.")
