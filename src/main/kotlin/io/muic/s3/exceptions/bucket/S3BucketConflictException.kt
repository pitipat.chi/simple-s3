package io.muic.s3.exceptions.bucket

import java.lang.RuntimeException

class S3BucketConflictException(name: String) : RuntimeException("Bucket '${name}' already exists.")
