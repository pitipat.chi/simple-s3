package io.muic.s3.exceptions.`object`

import java.lang.RuntimeException

class S3ObjectInvalidNameException(bucketName: String, objName: String) : RuntimeException("Object '/$bucketName/${objName}' name '${objName} is invalid.")
