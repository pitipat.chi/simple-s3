package io.muic.s3.exceptions.`object`

import java.lang.RuntimeException

class S3ObjectInvalidTicketException(bucketName: String, objName: String) : RuntimeException("Ticket for '${bucketName}/${objName}' is not yet closed.")

