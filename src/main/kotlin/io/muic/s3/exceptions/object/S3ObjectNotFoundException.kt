package io.muic.s3.exceptions.`object`

import java.lang.RuntimeException

class S3ObjectNotFoundException(bucketName: String, objName: String) : RuntimeException("Object '/$bucketName/${objName}' not found.")
