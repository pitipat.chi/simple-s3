package io.muic.s3.exceptions.`object`

import java.lang.RuntimeException

class S3ObjectInvalidRangeException(bucketName: String, objName: String) : RuntimeException("Object '${bucketName}/${objName}' already exists.")
