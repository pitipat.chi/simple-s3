package io.muic.s3.repositories

import io.muic.s3.entities.S3Bucket
import org.springframework.data.mongodb.repository.MongoRepository

interface S3BucketRepository : MongoRepository<S3Bucket, String>{
    fun findS3BucketByName(name: String): S3Bucket?
    fun deleteS3BucketByName(name: String)
    fun existsS3BucketByName(name: String): Boolean
}