package io.muic.s3.services

import io.muic.s3.entities.S3Bucket
import io.muic.s3.entities.S3Object
import io.muic.s3.entities.S3Part
import io.muic.s3.exceptions.`object`.S3ObjectInvalidRangeException
import io.muic.s3.exceptions.`object`.S3ObjectNotFoundException
import io.muic.s3.exceptions.`object`.S3ObjectInvalidTicketException
import io.muic.s3.exceptions.bucket.S3BucketNotFoundException
import io.muic.s3.exceptions.part.PartErrorDTO
import io.muic.s3.exceptions.part.S3PartNotFoundException
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream
import org.apache.pdfbox.io.RandomAccessInputStream
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.io.*
import java.util.*
import javax.servlet.http.HttpServletResponse
import kotlin.collections.RandomAccess

@Service
class S3ObjectDownloadService {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(S3ObjectDownloadService::class.java)
    }

    @Value("\${storage.location}")
    private lateinit var storageLoc: String

    @Autowired
    private lateinit var objectService: S3ObjectService

    private fun rangeTokenizer(range: String, length: Long): List<Pair<Long, Long>> {
        return range
                .replace(Regex("\\s*\\w+\\s*="), "")
                .split(",")
                .map { r ->
                    val p: List<String> = r.trim().split("-")
                    if (p.size < 2){
                        Pair<Long, Long>(0, p[0].toLong()-1)
                    }
                    else {
                        var pair: Pair<Long, Long> = Pair(0, 0)
                        pair = when (p[1] == "") {
                            true -> Pair(pair.first, length-1)
                            false -> Pair(pair.first, p[1].toLong())
                        }
                        pair = when (p[0] == "") {
                            true -> Pair(length-p[1].toLong(), length-1)
                            false -> Pair(p[0].toLong(), pair.second)
                        }
                        pair
                    }
                }
    }

    private fun downloadInRange(bucketName: String, objName: String, obj: S3Object, start: Long, end: Long): SequenceInputStream {
        val parts: Iterator<S3Part> = obj.parts.filter { !it.deleted }.sortedBy { it.partNumber }.iterator()
        val inputStreams: MutableList<RandomAccessBufferedFileInputStream> = mutableListOf()
        var length = 0L
        var skip = 0L
        for (part in parts) {
            if (length+part.length < start){
                length += part.length
                skip += part.length
                continue
            }
            logger.info("partNumber=${part.partNumber}")
            val path = part.path
            val file = File("$storageLoc/$path")
            if (!file.exists())
                throw S3PartNotFoundException(bucketName, objName, part.partNumber, PartErrorDTO(part.md5, part.length))
            val stream = RandomAccessBufferedFileInputStream(file)
            inputStreams.add(stream)
            length += part.length
            if (length > end)
                break
        }
        if (inputStreams.size > 0)
            inputStreams[0].seek(start-skip)
        return SequenceInputStream(Collections.enumeration(inputStreams))
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class, S3ObjectInvalidTicketException::class)
    fun download(bucketName: String, objName: String, range: String?, response: HttpServletResponse) {
        val (_, obj: S3Object) = objectService.find(bucketName, objName)
        if (obj.ticket)
            throw S3ObjectInvalidTicketException(bucketName, objName)
        if (obj.etag == null)
            throw S3ObjectNotFoundException(bucketName, objName)
        response.addHeader("ETag", "\"${obj.etag}\"")
        response.status = HttpStatus.OK.value()
        val rangeTokens: List<Pair<Long, Long>> = rangeTokenizer(range ?: "${obj.length}", obj.length)
        rangeTokens.forEach {
            if (it.first > obj.length || it.first > it.second)
                throw S3ObjectInvalidRangeException(bucketName, objName)

            IOUtils.copyLarge(
                    downloadInRange(bucketName, objName, obj, it.first, it.second),
                    response.outputStream,
                    0,
                    it.second - it.first + 1
            )
        }
    }

    fun eTagTokenizer(etags: String): List<String> {
        return etags
                .split(",")
                .map { it.trim().replace(Regex("['\"]+"), "") }
    }

    fun checkETags(bucketName: String, objName: String, eTags: String?): Boolean {
        if (eTags == null || eTags == "*")
            return false
        val (_, obj: S3Object) = objectService.find(bucketName, objName)
        return eTagTokenizer(eTags)
                .foldRight(false) { et, acc ->
                    obj.etag == et || acc
                }
    }
}