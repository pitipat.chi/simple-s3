package io.muic.s3.services

import io.muic.s3.entities.S3Bucket
import io.muic.s3.exceptions.bucket.S3BucketConflictException
import io.muic.s3.exceptions.bucket.S3BucketInvalidBucketNameException
import io.muic.s3.exceptions.bucket.S3BucketNotFoundException
import io.muic.s3.repositories.S3BucketRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Service
import java.io.File
import java.time.Instant

@Service
class S3BucketService {

    @Autowired
    private lateinit var bucketRepository: S3BucketRepository

    @Autowired
    private lateinit var mongoTemplate: MongoTemplate

    @Throws(S3BucketConflictException::class)
    fun create(name: String): S3Bucket {
        if (!isValidBucketName(name))
            throw S3BucketInvalidBucketNameException(name)
        if (exists(name))
            throw S3BucketConflictException(name)
        return if (bucketRepository.findS3BucketByName(name) == null) {
            bucketRepository.save(S3Bucket(name = name))
        } else {
            mongoTemplate.updateFirst(
                    Query(Criteria.where("name").`is`(name)),
                    Update().currentTimestamp("created")
                            .currentTimestamp("modified")
                            .set("deleted", false),
                    S3Bucket::class.java
            )
            bucketRepository.findS3BucketByName(name)!!
        }
    }

    @Throws(S3BucketNotFoundException::class)
    fun find(name: String): S3Bucket {
        if (!exists(name))
            throw S3BucketNotFoundException(name)
        return bucketRepository.findS3BucketByName(name)!!
    }

    @Throws(S3BucketNotFoundException::class)
    fun delete(name: String) {
        if (!exists(name))
            throw S3BucketNotFoundException(name)
        mongoTemplate.updateFirst(
                Query(Criteria.where("name").`is`(name)),
                Update().set("deleted", true)
                        .set("objects.\$[].deleted", true)
                        .set("objects.\$[].parts.\$[].deleted", true),
                S3Bucket::class.java
        )
    }

    fun exists(name: String): Boolean {
        return bucketRepository.existsS3BucketByName(name) && !bucketRepository.findS3BucketByName(name)!!.deleted
    }

    fun isValidBucketName(objName: String): Boolean {
        return objName.matches(Regex("^[A-Za-z0-9_-]+\$"))
    }

}