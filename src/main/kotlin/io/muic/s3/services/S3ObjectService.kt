package io.muic.s3.services

import io.muic.s3.entities.S3Bucket
import io.muic.s3.entities.S3Object
import io.muic.s3.entities.S3Part
import io.muic.s3.exceptions.`object`.S3ObjectConflictException
import io.muic.s3.exceptions.`object`.S3ObjectInvalidNameException
import io.muic.s3.exceptions.`object`.S3ObjectInvalidTicketException
import io.muic.s3.exceptions.`object`.S3ObjectNotFoundException
import io.muic.s3.exceptions.bucket.S3BucketNotFoundException
import io.muic.s3.exceptions.part.*
import io.muic.s3.repositories.S3BucketRepository
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import java.io.*
import java.lang.StringBuilder
import java.nio.charset.Charset
import java.security.DigestInputStream
import java.security.MessageDigest
import java.time.Instant
import java.util.*
import javax.xml.bind.DatatypeConverter

@Service
class S3ObjectService {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(S3ObjectService::class.java)
    }

    @Value("\${storage.location}")
    private lateinit var storageLoc: String

    @Autowired
    private lateinit var bucketRepository: S3BucketRepository

    @Autowired
    private lateinit var bucketService: S3BucketService

    @Autowired
    private lateinit var mongoTemplate: MongoTemplate

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class)
    internal fun find(bucketName: String, objName: String): Pair<S3Bucket, S3Object> {
        if (!bucketService.exists(bucketName))
            throw S3BucketNotFoundException(bucketName)
        if (!exists(bucketName, objName))
            throw S3ObjectNotFoundException(bucketName, objName)
        val bucket = bucketRepository.findS3BucketByName(bucketName)!!
        val obj = bucket.objects.find { it.name == objName }!!
        return Pair(bucket, obj)
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectConflictException::class)
    fun create(bucketName: String, objName: String) {
        if (!isValidObjectName(objName))
            throw S3ObjectInvalidNameException(bucketName, objName)
        if (!bucketService.exists(bucketName))
            throw S3BucketNotFoundException(bucketName)
        if (exists(bucketName, objName))
            throw S3ObjectConflictException(bucketName, objName)
        val bucket = bucketRepository.findS3BucketByName(bucketName) ?: throw S3BucketNotFoundException(bucketName)
        var obj: S3Object? = bucket.objects.find { it.name == objName }
        if (obj == null) {
            obj = S3Object(name = objName, created = Instant.now(), modified = Instant.now())
            mongoTemplate.updateFirst(
                    Query(Criteria.where("name").`is`(bucketName)),
                    Update().push("objects", obj),
                    S3Bucket::class.java
            )
        } else {
            mongoTemplate.updateFirst(
                    Query(Criteria.where("name").`is`(bucketName).and("objects.name").`is`(objName)),
                    Update().set("objects.\$.ticket", true)
                            .set("objects.\$.length", 0)
                            .set("objects.\$.etag", null)
                            .set("objects.\$.metadata", mapOf<String, Any>())
                            .currentTimestamp("objects.\$.created")
                            .currentTimestamp("objects.\$.modified")
                            .set("objects.\$.deleted", false),
                    S3Bucket::class.java
            )
        }
    }

    @Throws(
            S3PartInvalidBucketException::class,
            S3PartInvalidObjectException::class,
            S3PartInvalidObjectNameException::class,
            S3PartLengthMismatchedException::class,
            S3PartMD5MismatchedException::class,
            S3PartUnavailableTicketException::class,
            IOException::class
    )
    fun upload(bucketName: String, objName: String, partNumber: Int, reqLength: Long, reqMD5: String, stream: InputStream): S3Part {
        if (!isValidObjectName(objName))
            throw S3PartInvalidObjectNameException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength), null)
        var file: File? = null
        try {
            val uuid: String = UUID.randomUUID().toString()

            val dir1: String = uuid.slice(0..1)
            val dir2: String = uuid.slice(2..3)
            val dirs = File("$storageLoc/$dir1/$dir2")
            if (!dirs.exists())
                dirs.mkdirs()

            val path = "$dir1/$dir2/$uuid"
            file = File("$storageLoc/$path")
            if (file.exists())
                FileUtils.write(file, "", Charset.defaultCharset())
            else
                file.createNewFile()
            val (length, md5Hex) = writeToFile(stream, file)
            if (length != reqLength) {
                FileUtils.deleteQuietly(file)
                throw S3PartLengthMismatchedException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength))
            }
            if (md5Hex != reqMD5) {
                FileUtils.deleteQuietly(file)
                throw S3PartMD5MismatchedException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength))
            }

            val (_, obj: S3Object) = find(bucketName, objName)
            if (!obj.ticket){
                FileUtils.deleteQuietly(file)
                throw S3PartUnavailableTicketException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength))
            }
            if (partExists(bucketName, objName, partNumber)) {
                mongoTemplate.updateFirst(
                        Query(Criteria
                                .where("name").`is`(bucketName)
                                .and("objects.name").`is`(objName)),
                        Update()
                                .filterArray(Criteria.where("part.partNumber").`is`(partNumber)
                                        .and("part.deleted").`is`(false))
                                .set("objects.\$.parts.\$[part].deleted", true),
                        S3Bucket::class.java
                )
            }
            val part = S3Part(partNumber, length, md5Hex, path)
            mongoTemplate.updateFirst(
                    Query(Criteria
                            .where("name").`is`(bucketName)
                            .and("objects.name").`is`(objName)),
                    Update().push("objects.\$.parts", part)
                            .currentTimestamp("objects.\$.modified"),
                    S3Bucket::class.java
            )
            return part
        } catch (bnf: S3BucketNotFoundException) {
            if (file != null)
                FileUtils.deleteQuietly(file)
            throw S3PartInvalidBucketException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength), null)
        } catch (onf: S3ObjectNotFoundException) {
            if (file != null)
                FileUtils.deleteQuietly(file)
            throw S3PartInvalidObjectException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength), null)
        } catch (io : IOException) {
            if (file != null)
                FileUtils.deleteQuietly(file)
            throw S3PartInvalidContentException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength))
        }
    }

    @Throws(S3PartInvalidBucketException::class, S3PartInvalidObjectException::class, S3PartUnavailableTicketException::class, S3PartNotFoundException::class)
    fun deletePart(bucketName: String, objName: String, partNumber: Int) {
        try {
            val (_, obj: S3Object) = find(bucketName, objName)
            if (!obj.ticket)
                throw S3PartUnavailableTicketException(bucketName, objName, partNumber, null)
            if (partExists(bucketName, objName, partNumber)) {
                mongoTemplate.updateFirst(
                        Query(Criteria
                                .where("name").`is`(bucketName)
                                .and("objects.name").`is`(objName)),
                        Update()
                                .filterArray(Criteria.where("part.partNumber").`is`(partNumber)
                                        .and("part.deleted").`is`(false))
                                .set("objects.\$.parts.\$[part].deleted", true),
                        S3Bucket::class.java
                )
            } else {
                throw S3PartNotFoundException(bucketName, objName, partNumber, null)
            }
        } catch (bnf: S3BucketNotFoundException) {
            throw S3PartInvalidBucketException(bucketName, objName, partNumber, null, null)
        } catch (onf: S3ObjectNotFoundException) {
            throw S3PartInvalidObjectException(bucketName, objName, partNumber, null, null)
        }
    }

    @Throws(S3PartInvalidBucketException::class, S3PartInvalidObjectException::class, S3ObjectInvalidTicketException::class)
    fun complete(bucketName: String, objName: String): S3Object {
        try {
            val (_, obj: S3Object) = find(bucketName, objName)
            if (!obj.ticket)
                throw S3ObjectInvalidTicketException(bucketName, objName)
            val parts = obj.parts.filter { !it.deleted }
            obj.ticket = false
            obj.length = parts.fold(0) { acc: Long, s3Part: S3Part -> acc + s3Part.length }
            obj.etag = "${DigestUtils.md5Hex(parts.fold(StringBuilder()) { acc: StringBuilder, s3Part: S3Part -> acc.append(s3Part.md5) }.toString())}-${parts.size}"
            mongoTemplate.updateFirst(
                    Query(Criteria.where("name").`is`(bucketName).and("objects.name").`is`(objName)),
                    Update().set("objects.\$.ticket", obj.ticket)
                            .set("objects.\$.length", obj.length)
                            .set("objects.\$.etag", obj.etag)
                            .currentTimestamp("objects.\$.modified"),
                    S3Bucket::class.java
            )
            return obj
        } catch (bnf: S3BucketNotFoundException) {
            throw S3PartInvalidBucketException(bucketName, objName, 0, null, ObjectErrorDTO(bucketName, objName))
        } catch (onf: S3ObjectNotFoundException) {
            throw S3PartInvalidObjectException(bucketName, objName, 0, null, ObjectErrorDTO(bucketName, objName))
        }
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class)
    fun delete(bucketName: String, objName: String) {
        find(bucketName, objName)
        mongoTemplate.updateFirst(
                Query(Criteria.where("name").`is`(bucketName)
                        .and("objects.name").`is`(objName)),
                Update().set("objects.\$.deleted", true)
                        .set("objects.\$.parts.\$[].deleted", true),
                S3Bucket::class.java
        )
    }

    fun exists(bucketName: String, objName: String): Boolean {
        val bucket = bucketRepository.findS3BucketByName(bucketName) ?: return false
        return bucket.objects.find { it.name == objName } != null &&
                !bucket.objects.find { it.name == objName }!!.deleted
    }

    fun partExists(bucketName: String, objName: String, partNumber: Int): Boolean {
        return try {
            val (_, obj: S3Object) = find(bucketName, objName)
            val part = obj.parts.find { it.partNumber == partNumber && !it.deleted }
            part != null
        } catch (bnf: S3BucketNotFoundException) {
            false
        } catch (onf: S3ObjectNotFoundException) {
            false
        }
    }

    fun isValidObjectName(objName: String): Boolean {
        return objName.matches(Regex("^[A-Za-z0-9_.-]+(?<!\\.)\$"))
    }

    @Throws(IOException::class)
    fun writeToFile(stream: InputStream, file: File): Pair<Long, String> {
        var fileOut: FileOutputStream? = null
        var digestInputStream: DigestInputStream? = null
        try {
            val md5: MessageDigest = DigestUtils.getMd5Digest()
            fileOut = FileOutputStream(file)
            digestInputStream = DigestInputStream(stream, md5)
            val length: Long = IOUtils.copyLarge(digestInputStream, fileOut)
            val md5Hex = DatatypeConverter.printHexBinary(md5.digest()).toLowerCase()
            return Pair(length, md5Hex)
        } finally {
            fileOut?.close()
            digestInputStream?.close()
            stream.close()
        }
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class)
    fun updateKey(bucketName: String, objName: String, key: String, value: Any) {
        find(bucketName, objName)
        mongoTemplate.updateFirst(
                Query(Criteria
                        .where("name").`is`(bucketName)
                        .and("objects.name").`is`(objName)),
                Update()
                        .set("objects.\$.metadata.$key", value)
                        .currentTimestamp("objects.\$.modified"),
                S3Bucket::class.java
        )
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class)
    fun getKey(bucketName: String, objName: String, key: String): Map<String, Any> {
        val (_, obj: S3Object) = find(bucketName, objName)
        return obj.metadata.filter { it.key == key }
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class)
    fun deleteKey(bucketName: String, objName: String, key: String) {
        find(bucketName, objName)
        mongoTemplate.updateFirst(
                Query(Criteria
                        .where("name").`is`(bucketName)
                        .and("objects.name").`is`(objName)),
                Update().unset("objects.\$.metadata.$key")
                        .currentTimestamp("objects.\$.modified"),
                S3Bucket::class.java
        )
    }

    @Throws(S3BucketNotFoundException::class, S3ObjectNotFoundException::class)
    fun getAll(bucketName: String, objName: String): Map<String, Any> {
        val (_, obj: S3Object) = find(bucketName, objName)
        return obj.metadata
    }

}