package io.muic.s3

import io.muic.s3.entities.S3Base
import io.muic.s3.entities.S3Bucket
import io.muic.s3.entities.S3Object
import io.muic.s3.repositories.S3BucketRepository
import org.apache.commons.io.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.io.File
import java.util.*

@Configuration
@EnableScheduling
class GarbageCollector {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(GarbageCollector::class.java)
    }

    @Value("\${storage.location}")
    private lateinit var storageLoc: String

    @Autowired
    private lateinit var bucketRepository: S3BucketRepository

    @Scheduled(cron = "0 * * ? * *")
    fun collect() {
        logger.info("Start Garbage Collecting")
        val ad = bucketRepository.findAll().foldRight(0) { bucket, bacc ->
            val bd = bucket.objects.foldRight(0) { obj, oacc ->
                val od = obj.parts.foldRight(0) { part, pacc ->
                    var pd = 0
                    if (part.deleted) {
                        val file = File("$storageLoc/${part.path}")
                        pd = 1
                        FileUtils.deleteQuietly(file)
                    }
                    pacc + pd
                }
                obj.parts = obj.parts.filter { !it.deleted }
                oacc + od
            }
            bucket.objects = bucket.objects.filter { !it.deleted }
            if (bucket.deleted)
                bucketRepository.deleteS3BucketByName(bucket.name)
            else
                bucketRepository.save(bucket)
            bacc + bd
        }
        logger.info("End Garbage Collecting ($ad files deleted)")
    }

}