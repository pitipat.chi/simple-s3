package io.muic.s3.controllers

import io.muic.s3.entities.S3Part
import io.muic.s3.entities.dto.S3ObjectDTO
import io.muic.s3.entities.dto.toDTO
import io.muic.s3.exceptions.part.*
import io.muic.s3.services.S3ObjectDownloadService
import io.muic.s3.services.S3ObjectService
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class S3ObjectController {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(S3ObjectController::class.java)
    }

    @Autowired
    private lateinit var objService: S3ObjectService

    @Autowired
    private lateinit var downloadService: S3ObjectDownloadService

    @PostMapping("/{bucketName}/{objName}", params = ["create"])
    fun create(@PathVariable bucketName: String, @PathVariable objName: String) {
        objService.create(bucketName, objName)
    }

    @DeleteMapping("/{bucketName}/{objName}", params = ["delete"])
    fun delete(@PathVariable bucketName: String , @PathVariable objName: String) {
        objService.delete(bucketName, objName)
    }

    @PutMapping("/{bucketName}/{objName}", params = ["partNumber"])
    fun upload(@PathVariable bucketName: String, @PathVariable objName: String,
               @RequestParam("partNumber") partNumber: Int,
               @RequestHeader("Content-Length", required = true) reqLength: Long,
               @RequestHeader("Content-MD5", required = true) reqMD5: String,
               req: HttpServletRequest): S3Part {
        if (partNumber < 1 || partNumber > 10000)
            throw S3PartInvalidPartNumberException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength))
        when (req.inputStream) {
            null -> throw S3PartInvalidContentException(bucketName, objName, partNumber, PartErrorDTO(reqMD5, reqLength))
            else -> return objService.upload(bucketName, objName, partNumber, reqLength, reqMD5, req.inputStream)
        }

    }

    @DeleteMapping("/{bucketName}/{objName}", params = ["partNumber"])
    fun deletePart(@PathVariable bucketName: String, @PathVariable objName: String, @RequestParam("partNumber") partNumber: Int) {
        objService.deletePart(bucketName, objName, partNumber)
    }

    @PostMapping("/{bucketName}/{objName}", params = ["complete"])
    fun complete(@PathVariable bucketName: String, @PathVariable objName: String): S3ObjectDTO {
        return objService.complete(bucketName, objName).toDTO()
    }

    @GetMapping("/{bucketName}/{objName}")
    fun download(@PathVariable bucketName: String, @PathVariable objName: String,
                 @RequestHeader("Range", required = false) range: String?,
                 @RequestHeader("If-None-Match", required = false) reqETags: String?,
                 response: HttpServletResponse){
        if (downloadService.checkETags(bucketName, objName, reqETags))
            response.status = HttpStatus.NOT_MODIFIED.value()
        else {
            downloadService.download(bucketName, objName, range, response)
        }
    }
}