package io.muic.s3.controllers

import io.muic.s3.entities.S3Bucket
import io.muic.s3.entities.dto.S3BucketDTO
import io.muic.s3.entities.dto.toDTO
import io.muic.s3.services.S3BucketService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class S3BucketController {

    @Autowired
    private lateinit var bucketService: S3BucketService

    @PostMapping("/{bucketName}", params = ["create"])
    fun create(@PathVariable bucketName: String): ResponseEntity<Any> {
        val ret: MutableMap<String, Any> = mutableMapOf()
        val bucket = bucketService.create(bucketName)
        ret["name"] = bucket.name
        ret["created"] = bucket.created!!.toEpochMilli()
        ret["modified"] = bucket.modified!!.toEpochMilli()
        return ResponseEntity(ret, HttpStatus.OK)
    }

    @DeleteMapping("/{bucketName}", params = ["delete"])
    fun delete(@PathVariable bucketName: String) {
        bucketService.delete(bucketName)
    }

    @GetMapping("/{bucketName}", params = ["list"])
    fun list(@PathVariable bucketName: String): S3BucketDTO {
        return bucketService.find(bucketName).toDTO()
    }
}