package io.muic.s3.controllers.advisors

import io.muic.s3.exceptions.bucket.S3BucketConflictException
import io.muic.s3.exceptions.bucket.S3BucketNotFoundException
import io.muic.s3.exceptions.part.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class S3PartAdvisor: ResponseEntityExceptionHandler() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(S3PartAdvisor::class.java)
    }

    @ExceptionHandler(value = [
        S3PartInvalidBucketException::class,
        S3PartInvalidObjectException::class,
        S3PartInvalidObjectNameException::class,
        S3PartInvalidPartNumberException::class,
        S3PartInvalidContentException::class,
        S3PartLengthMismatchedException::class,
        S3PartMD5MismatchedException::class,
        S3PartUnavailableTicketException::class,
        S3PartNotFoundException::class
    ])
    protected fun invalidPart(ex: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        val s3ex = ex as S3PartException
        val ret: MutableMap<String, Any> = mutableMapOf()
        if (s3ex.partError != null) {
            ret["md5"] = s3ex.partError.md5
            ret["length"] = s3ex.partError.length
            ret["partNumber"] = s3ex.partNumber
        }
        if (s3ex.objError != null) {
            ret["bucketName"] = s3ex.objError.bucket
            ret["name"] = s3ex.objError.name
        }
        ret["error"] = s3ex::class.java
                .simpleName
                .replace("S3Part", "")
                .replace("Exception", "")
        return handleExceptionInternal(ex, ret, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }

}