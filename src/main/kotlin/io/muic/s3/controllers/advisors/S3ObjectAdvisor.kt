package io.muic.s3.controllers.advisors

import io.muic.s3.exceptions.`object`.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class S3ObjectAdvisor: ResponseEntityExceptionHandler() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(S3ObjectAdvisor::class.java)
    }

    @ExceptionHandler(value = [
        S3ObjectNotFoundException::class,
        S3ObjectConflictException::class,
        S3ObjectInvalidNameException::class,
        S3ObjectInvalidTicketException::class,
        S3ObjectInvalidRangeException::class
    ])
    protected fun invalidObjectName(ex: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }

}