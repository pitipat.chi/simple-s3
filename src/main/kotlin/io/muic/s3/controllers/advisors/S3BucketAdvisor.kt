package io.muic.s3.controllers.advisors

import com.mongodb.MongoWriteException
import io.muic.s3.exceptions.bucket.S3BucketConflictException
import io.muic.s3.exceptions.bucket.S3BucketInvalidBucketNameException
import io.muic.s3.exceptions.bucket.S3BucketNotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class S3BucketAdvisor: ResponseEntityExceptionHandler() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(S3BucketAdvisor::class.java)
    }

    @ExceptionHandler(value = [
        S3BucketNotFoundException::class,
        S3BucketConflictException::class,
        S3BucketInvalidBucketNameException::class,
        MongoWriteException::class
    ])
    protected fun invalidBucketName(ex: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }

}