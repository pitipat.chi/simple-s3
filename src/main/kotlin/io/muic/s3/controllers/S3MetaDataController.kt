package io.muic.s3.controllers

import io.muic.s3.services.S3BucketService
import io.muic.s3.services.S3ObjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class S3MetaDataController {

    @Autowired
    private lateinit var objectService: S3ObjectService

    @PutMapping("/{bucketName}/{objectName}", params = ["metadata", "key"])
    fun updateKey(@PathVariable bucketName: String, @PathVariable objectName: String,
               @RequestParam("key") key: String, @RequestBody value: String) : ResponseEntity<Any> {
        if (!objectService.exists(bucketName, objectName))
            return ResponseEntity(HttpStatus.NOT_FOUND)
        objectService.updateKey(bucketName, objectName, key, value)
        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping("/{bucketName}/{objectName}", params = ["metadata", "key"])
    fun getKey(@PathVariable bucketName: String, @PathVariable objectName: String,
               @RequestParam("key") key: String): ResponseEntity<Map<String, Any>> {
        if (!objectService.exists(bucketName, objectName))
            return ResponseEntity(HttpStatus.NOT_FOUND)
        return ResponseEntity(objectService.getKey(bucketName, objectName, key), HttpStatus.OK)
    }

    @DeleteMapping("/{bucketName}/{objectName}", params = ["metadata", "key"])
    fun deleteKey(@PathVariable bucketName: String, @PathVariable objectName: String,
               @RequestParam("key") key: String): ResponseEntity<Any> {
        if (!objectService.exists(bucketName, objectName))
            return ResponseEntity(HttpStatus.NOT_FOUND)
        objectService.deleteKey(bucketName, objectName, key)
        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping("/{bucketName}/{objectName}", params = ["metadata"])
    fun get(@PathVariable bucketName: String, @PathVariable objectName: String): ResponseEntity<Map<String, Any>> {
        if (!objectService.exists(bucketName, objectName))
            return ResponseEntity(HttpStatus.NOT_FOUND)
        return ResponseEntity(objectService.getAll(bucketName, objectName), HttpStatus.OK)
    }

}