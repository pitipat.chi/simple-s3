package io.muic.s3.configurations

import com.mongodb.WriteConcern
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.MongoAction
import org.springframework.data.mongodb.core.WriteConcernResolver


@Configuration
class MongoDBConfiguration {
    @Bean
    fun writeConcernResolver(): WriteConcernResolver? {
        return WriteConcernResolver { action: MongoAction? ->
            println("Using Write Concern of Acknowledged")
            WriteConcern.ACKNOWLEDGED
        }
    }
}