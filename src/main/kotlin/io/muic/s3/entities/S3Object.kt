package io.muic.s3.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.time.Instant

class S3Object (
    var name: String,
    @JsonIgnore
    var ticket: Boolean = true,
    var length: Long = 0,
    var etag: String? = null,
    @JsonIgnore
    var parts: List<S3Part> = listOf(),
    @JsonIgnore
    var metadata: Map<String, Any> = mapOf(),
    @CreatedDate
    var created: Instant? = null,
    @LastModifiedDate
    var modified: Instant? = null,
    @JsonIgnore
    var deleted: Boolean = false
) : S3Base()

