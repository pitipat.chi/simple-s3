package io.muic.s3.entities.dto

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.Instant

data class S3BucketDTO (
        var name: String,
        @JsonFormat(shape = JsonFormat.Shape.NUMBER, without = [JsonFormat.Feature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS])
        var created: Instant? = null,
        @JsonFormat(shape = JsonFormat.Shape.NUMBER, without = [JsonFormat.Feature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS])
        var modified: Instant? = null,
        var objects: List<S3ObjectDTO> = mutableListOf()
)