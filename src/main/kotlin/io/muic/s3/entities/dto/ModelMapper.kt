package io.muic.s3.entities.dto

import io.muic.s3.entities.S3Bucket
import io.muic.s3.entities.S3Object
import kotlin.reflect.full.memberProperties

fun S3Bucket.toDTO() = kotlin.with(::S3BucketDTO) {
    val propName = S3Bucket::class.memberProperties.associateBy { it.name }
    callBy(parameters.associate { p ->
        p to when (p.name) {
            S3BucketDTO::objects.name -> objects.filter { !it.deleted }.map { it.toDTO() }
            else -> propName[p.name]?.get(this@toDTO)
        }
    })
}

fun S3Object.toDTO() = kotlin.with(::S3ObjectDTO) {
    val propName = S3Object::class.memberProperties.associateBy { it.name }
    callBy(parameters.associate { p ->
        p to when (p.name) {
            else -> propName[p.name]?.get(this@toDTO)
        }
    })
}

