package io.muic.s3.entities

import com.fasterxml.jackson.annotation.JsonIgnore

class S3Part (
        var partNumber: Int,
        var length: Long,
        var md5: String,
        @JsonIgnore
        var path: String,
        @JsonIgnore
        var deleted: Boolean = false
) : S3Base()