package io.muic.s3.entities

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.index.Indexed
import java.time.Instant

class S3Bucket (
    @Id
    var name: String,
    @JsonFormat(shape = JsonFormat.Shape.NUMBER, without = [JsonFormat.Feature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS])
    @CreatedDate
    var created: Instant? = null,
    @JsonFormat(shape = JsonFormat.Shape.NUMBER, without = [JsonFormat.Feature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS])
    @LastModifiedDate
    var modified: Instant? = null,
    var objects: List<S3Object> = mutableListOf(),
    @JsonIgnore
    var deleted: Boolean = false,
    @JsonIgnore
    @Version
    var version: Long = 0
) : S3Base()
